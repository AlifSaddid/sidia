from django.db.utils import IntegrityError, InternalError
from django.shortcuts import redirect, render
from django.db import connection

# See 1 (Done)
def login(request):
    try:
        if (request.session['username'] != ''):
            return redirect ('/')    
    except:
        pass
    if (request.method == 'POST'):
        return loginUser(request)
    else:
        return showLogin(request)

def showLogin(request):
    return render(request, 'auth/login.html')

def loginUser(request):
    username = request.POST['username']
    password = request.POST['password']
    role = None
    try:
        with connection.cursor() as c:
            c.execute('''
                SELECT * FROM Pengguna WHERE username = '{}' AND password = '{}'
            '''.format(username, password))
            data_user = c.fetchone()
            if (data_user != None):
                c.execute(f"SELECT * FROM Admin_Satgas WHERE username = '{username}'")
                admin = c.fetchone()
                c.execute(f"SELECT * FROM Petugas_Faskes WHERE username = '{username}'")
                faskes = c.fetchone()
                c.execute(f"SELECT * FROM Supplier WHERE username = '{username}'")
                supplier = c.fetchone()
                c.execute(f"SELECT * FROM Petugas_Distribusi WHERE username = '{username}'")
                distribusi = c.fetchone()

                if (admin):
                    role = 'Admin Satgas'
                elif (faskes):
                    role = 'Petugas Faskes'
                elif (supplier):
                    role = 'Supplier'
                elif (distribusi):
                    role = 'Petugas Distribusi'

                request.session['username'] = username
                request.session['role'] = role
                
                return redirect('/')
    except:
        pass

    return render(request, 'auth/login.html')

# See 2 (Done)
def register(request):
    try:
        if (request.session['username'] != ''):
            return redirect ('/')    
    except:
        pass
    if (request.method == "POST"):
        return registerUser(request)
    else:
        return showRegister(request)
    
def showRegister(request):
    return render(request, 'auth/register.html')

def registerUser(request):
    data = request.POST
    tipe = data['tipe']
    username = data['username']
    password = data['password']
    fullname = data['fullname']
    kelurahan = data['alamat-kelurahan']
    kecamatan = data['alamat-kecamatan']
    kabupaten = data['alamat-kabupaten']
    provinsi = data['alamat-provinsi']
    notelp = data['no-telp']
    organisasi = data['organisasi']
    nosim = data['no-sim']
    context = {}
    try:
        with connection.cursor() as c:
            c.execute('''
                INSERT INTO PENGGUNA(Username, Nama, Password, Alamat_Kel, Alamat_Kec, Alamat_KabKot, Alamat_Prov, No_telepon)
                VALUES ('{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}')
            '''.format(username, fullname, password, kelurahan, kecamatan, kabupaten, provinsi, notelp))
            if (tipe == 'admin'):
                c.execute('''
                    INSERT INTO ADMIN_SATGAS(Username)
                    VALUES ('{}')
                '''.format(username))   
            elif (tipe == 'faskes'):
                c.execute('''
                    INSERT INTO PETUGAS_FASKES(Username)
                    VALUES ('{}')
                '''.format(username)) 
            elif (tipe == 'supplier'):
                c.execute('''
                    INSERT INTO SUPPLIER(Username, Nama_organisasi)
                    VALUES ('{}', '{}')
                '''.format(username, organisasi)) 
            elif (tipe == 'distribusi'):
                c.execute('''
                    INSERT INTO PETUGAS_DISTRIBUSI(Username, NO_SIM)
                    VALUES ('{}', '{}')
                '''.format(username, nosim))
    except IntegrityError:
        pass
    except InternalError:
        context['duplicate_error'] = True
        context['username'] = username
        with connection.cursor() as c:
            c.execute(f"SELECT * FROM Admin_Satgas WHERE username = '{username}'")
            admin = c.fetchone()
            c.execute(f"SELECT * FROM Petugas_Faskes WHERE username = '{username}'")
            faskes = c.fetchone()
            c.execute(f"SELECT * FROM Supplier WHERE username = '{username}'")
            supplier = c.fetchone()
            c.execute(f"SELECT * FROM Petugas_Distribusi WHERE username = '{username}'")
            distribusi = c.fetchone()

            if (admin):
                role = 'Admin Satgas'
            elif (faskes):
                role = 'Petugas Faskes'
            elif (supplier):
                role = 'Supplier'
            elif (distribusi):
                role = 'Petugas Distribusi'
            context['role'] = role
                
        return render(request, 'auth/register.html', context)
    return redirect('/authentication/login')

# See 1 (Done)
def logout(request):
    request.session['username'] = ''
    request.session['role'] = ''
    return redirect('/authentication/login')