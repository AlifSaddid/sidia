from django.shortcuts import render

# See 13c
def new(request):
    return render(request, 'warehouse/new.html')

# See 13d
def all(request):
    return render(request, 'warehouse/all.html')

# See 13e
def update(request, id):
    return render(request, 'warehouse/update.html')

# See 13f
def delete(request, id):
    return render(request, 'warehouse/all.html')