from datetime import datetime
from django.db import connection
from django.shortcuts import redirect, render
import json

# See 6c (Done)
def new(request):
    try:
        if (request.session['username'] == ''):
            return redirect ('/authentication/login')    
    except:
        return redirect ('/authentication/login')
    if (request.method == 'GET'):
        return displayNew(request)
    else:
        return saveNew(request)

def displayNew(request):
    context = {
        'next_id': get_next_id_pesanan(),
        'suppliers': get_all_supplier(),
        'items': convert_to_list(get_all_item_sumber_daya())
    }
    return render(request, 'pesanan/new.html', context)

def saveNew(request):
    context = {}
    data = request.POST
    items = json.loads(data['items'])
    admin = data['petugas']
    supplier = data['supplier']
    nomor_transaksi = data['nomor']
    date_now = datetime.today().strftime('%Y-%m-%d')
    with connection.cursor() as c:
        c.execute('''
            INSERT INTO TRANSAKSI_SUMBER_DAYA(Nomor, Tanggal, Total_berat, Total_Item)
            VALUES ({}, '{}', {}, {})
        '''.format(nomor_transaksi, date_now, 0, 0))

        c.execute('''
            INSERT INTO PESANAN_SUMBER_DAYA(Nomor_pesanan, Username_admin_satgas, Total_Harga)
            VALUES ({}, '{}', {})
        '''.format(nomor_transaksi, admin, 0))

        c.execute('''
            INSERT INTO RIWAYAT_STATUS_PESANAN(kode_status_pesanan, no_pesanan, username_supplier, tanggal)
            VALUES ('{}', {}, '{}', '{}')
        '''.format('REQ-SUP', nomor_transaksi, supplier, date_now))

        for item in items:
            kode = item[0]
            supplier = item[1]
            no_urut = item[7]
            jumlah_item = item[6]

            c.execute('''
                INSERT INTO DAFTAR_ITEM(No_transaksi_sumber_daya, No_urut, Jumlah_Item, Kode_item_sumber_daya)
                VALUES ({}, {}, {}, '{}')
            '''.format(nomor_transaksi, no_urut, jumlah_item, kode))

    return render(request, 'pesanan/new.html', context)

# See 6d (Done)
def all(request):
    try:
        if (request.session['username'] == ''):
            return redirect ('/authentication/login')    
    except:
        return redirect ('/authentication/login')
    context = {}
    role = request.session['role']
    username = request.session['username']
    if (role != ''):
        if (role == 'Admin Satgas'):
            return all_admin(request, username)
        elif (role == 'Supplier'):
            return all_supplier(request, username)
        return render(request, 'pesanan/all.html', context)
    else:
        return redirect('/authentication/login')

def all_supplier(request, supplier):
    context = {}
    all_pesanan = get_all_pesanan_by_supplier(supplier)
    data_pesanan = []
    for pesanan in all_pesanan:
        status = None
        with connection.cursor() as c:
            c.execute('''
                SELECT * FROM riwayat_status_pesanan
                WHERE no_pesanan = {}
            '''.format(pesanan[0]))
            status = c.fetchall()[-1]
        new_pesanan = list(pesanan)
        new_pesanan.extend(status)
        data_pesanan.append(new_pesanan)
    context['all_pesanan'] = data_pesanan
    return render(request, 'pesanan/all.html', context)

def all_admin(request, admin):
    context = {}
    all_pesanan = get_all_pesanan_by_admin(admin)
    data_pesanan = []
    for pesanan in all_pesanan:
        status = None
        with connection.cursor() as c:
            c.execute('''
                SELECT * FROM riwayat_status_pesanan
                WHERE no_pesanan = {}
            '''.format(pesanan[0]))
            status = c.fetchall()[-1]
        new_pesanan = list(pesanan)
        new_pesanan.extend(status)
        data_pesanan.append(new_pesanan)
    context['all_pesanan'] = data_pesanan
    return render(request, 'pesanan/all.html', context)

# See 6d (Done)
def detail(request, id):
    try:
        if (request.session['username'] == ''):
            return redirect ('/authentication/login')    
    except:
        return redirect ('/authentication/login')
    context = {}
    with connection.cursor() as c:
        c.execute('''
            SELECT * FROM pesanan_sumber_daya psd
            LEFT JOIN transaksi_sumber_daya tsd
                ON psd.nomor_pesanan = tsd.nomor
            LEFT JOIN daftar_item di
                ON di.no_transaksi_sumber_daya = tsd.nomor
            LEFT JOIN item_sumber_daya isd
	            ON isd.kode = di.kode_item_sumber_daya
            WHERE tsd.nomor = {}
        '''.format(int(id)))
        context['pesanan'] = c.fetchall() 
    return render(request, 'pesanan/detail.html', context)

# See 6e (Done)
def update(request, id):
    try:
        if (request.session['username'] == ''):
            return redirect ('/authentication/login')    
    except:
        return redirect ('/authentication/login')
    if (request.method == 'GET'):
        return display_update(request, id)
    elif (request.method == 'POST'):
        return save_update(request, id)
    

def display_update(request, id):
    context = {}
    with connection.cursor() as c:
        c.execute('''
            SELECT * FROM pesanan_sumber_daya psd
            LEFT JOIN transaksi_sumber_daya tsd
                ON psd.nomor_pesanan = tsd.nomor
            LEFT JOIN daftar_item di
                ON di.no_transaksi_sumber_daya = tsd.nomor
            LEFT JOIN item_sumber_daya isd
	            ON isd.kode = di.kode_item_sumber_daya
            WHERE tsd.nomor = {}
        '''.format(int(id)))
        context['next_nomor_urut'] = get_next_nomor_urut(int(id))
        context['pesanan'] = c.fetchall()
        context['items'] = convert_to_list(get_all_daftar_item(int(id)))
        context['all_items'] = convert_to_list(get_all_item_sumber_daya())
    return render(request, 'pesanan/update.html', context)

def save_update(request, id):
    data = request.POST
    items = json.loads(data['items'])
    nomor = int(id)
    with connection.cursor() as c:
        c.execute('''
            DELETE FROM daftar_item
            WHERE no_transaksi_sumber_daya = {}
        '''.format(nomor))

        for item in items:
            kode = item[0]
            no_urut = item[7]
            jumlah_item = item[6]

            c.execute('''
                INSERT INTO DAFTAR_ITEM(No_transaksi_sumber_daya, No_urut, Jumlah_Item, Kode_item_sumber_daya)
                VALUES ({}, {}, {}, '{}')
            '''.format(nomor, no_urut, jumlah_item, kode))

    return render(request, 'pesanan/update.html')

# See 6f (Done)
def delete(request, id):
    try:
        if (request.session['username'] == ''):
            return redirect ('/authentication/login')    
    except:
        return redirect ('/authentication/login')
    nomor = int(id)
    with connection.cursor() as c:
        c.execute('''
            DELETE FROM riwayat_status_pesanan
            WHERE no_pesanan = {}
        '''.format(nomor))

        c.execute('''
            DELETE FROM daftar_item
            WHERE no_transaksi_sumber_daya = {}
        '''.format(nomor))

        c.execute('''
            DELETE FROM pesanan_sumber_daya
            WHERE nomor_pesanan = {}
        '''.format(nomor))

        c.execute('''
            DELETE FROM transaksi_sumber_daya
            WHERE nomor = {}
        '''.format(nomor))
    return render(request, 'pesanan/all.html')

# See 7c (Done)
def statuscreate(request, id):
    try:
        if (request.session['username'] == ''):
            return redirect ('/authentication/login')    
    except:
        return redirect ('/authentication/login')
    status = request.POST['status']
    username = request.session['username']
    date_now = datetime.today().strftime('%Y-%m-%d')
    with connection.cursor() as c:
        c.execute('''
            INSERT INTO RIWAYAT_STATUS_PESANAN(kode_status_pesanan, no_pesanan, username_supplier, tanggal)
            VALUES ('{}', {}, '{}', '{}')
        '''.format(status, int(id), username, date_now))
    return render(request, 'pesanan/all.html')

# See 7d  (Done)
def status(request, id):
    try:
        if (request.session['username'] == ''):
            return redirect ('/authentication/login')    
    except:
        return redirect ('/authentication/login')
    context = {}
    with connection.cursor() as c:
        c.execute('''
                SELECT * FROM riwayat_status_pesanan rsp
                LEFT JOIN status_pesanan sp
                    ON rsp.kode_status_pesanan = sp.kode
                WHERE no_pesanan = {}
            '''.format(int(id)))
        context['statuses'] = convert_to_list(c.fetchall())
        context['id'] = id
    return render(request, 'pesanan/status.html', context)


# Utils
def get_all_supplier():
    with connection.cursor() as c:
        c.execute('''
            SELECT * FROM supplier
        ''')
        
        return c.fetchall()

def get_all_item_sumber_daya():
    with connection.cursor() as c:
        c.execute('''
            SELECT * FROM item_sumber_daya
        ''')
        
        return c.fetchall()

def get_all_pesanan_by_supplier(username_supplier):
    with connection.cursor() as c:
        c.execute('''
            SELECT DISTINCT psd.*, tsd.*
            FROM riwayat_status_pesanan rsp
            LEFT JOIN pesanan_sumber_daya psd
                ON psd.nomor_pesanan = rsp.no_pesanan
            LEFT JOIN transaksi_sumber_daya tsd
                ON psd.nomor_pesanan = tsd.nomor
            WHERE username_supplier = '{}'
        '''.format(username_supplier))
        return c.fetchall()

def get_all_pesanan_by_admin(username_admin):
    with connection.cursor() as c:
        c.execute('''
            SELECT * FROM pesanan_sumber_daya psd
            LEFT JOIN transaksi_sumber_daya tsd
                ON psd.nomor_pesanan = tsd.nomor
            WHERE username_admin_satgas = '{}'
        '''.format(username_admin))
        return c.fetchall()

def get_all_pesanan():
    with connection.cursor() as c:
        c.execute('''
            SELECT * FROM pesanan_sumber_daya psd
            LEFT JOIN transaksi_sumber_daya tsd
                ON psd.nomor_pesanan = tsd.nomor
        ''')
        return c.fetchall()

def get_all_transaksi():
    with connection.cursor() as c:
        c.execute('SELECT * FROM transaksi_sumber_daya')
        return c.fetchall()

def get_next_id_pesanan():
    all_pesanan = get_all_transaksi()
    max_id = 0
    for pesanan in all_pesanan:
        if (pesanan[0] > max_id):
            max_id = pesanan[0]
    return max_id + 1

def get_all_daftar_item(nomor):
    with connection.cursor() as c:
        c.execute('''
            SELECT * FROM daftar_item
            WHERE no_transaksi_sumber_daya = {}
        '''.format(nomor))

        return c.fetchall()

def get_next_nomor_urut(id_pesanan):
    with connection.cursor() as c:
        c.execute('''
            SELECT no_urut FROM daftar_item
            WHERE no_transaksi_sumber_daya = {}
        '''.format(id_pesanan))
        max_id = 0
        for item in c.fetchall():
            if (item[0] > max_id):
                max_id = item[0]
        return max_id + 1

def convert_to_list(tuples):
    result = []
    for data in tuples:
        result.append(list(data))
    return result
