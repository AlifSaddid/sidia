from django.urls import path
from . import views

app_name = 'pesanan'

urlpatterns = [
    path('new/', views.new, name = 'new'),
    path('detail/<str:id>', views.detail, name = 'detail'),
    path('update/<str:id>', views.update, name = 'update'),
    path('delete/<str:id>', views.delete, name = 'delete'),
    path('status/create/<str:id>', views.statuscreate, name = 'statuscreate'),
    path('status/<str:id>', views.status, name = 'status'),
    path('', views.all, name = 'all')
]