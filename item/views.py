from django.shortcuts import render, redirect
from django.db import connection
import json

# See 5c
def new(request):
    return render(request, 'item/new.html')

# See 5d
def all(request):
    return render(request, 'item/all.html')

# See 5e
#Fetch data done, Update data berhasil di local ()
def update(request, id):
    data = []
    context = {}
    print(request.session["role"])
    if(request.session["role"] == "Supplier"):
        if(request.method == "POST"):
            namaItem = request.POST["namaItem"]
            beratSatuan = request.POST["beratSatuan"]
            hargaSatuan = request.POST["hargaSatuan"]
            kodeTipeItem = request.POST["kodeTipeItem"]

            with connection.cursor() as c:
                command = f"UPDATE ITEM_SUMBER_DAYA SET nama = '{namaItem}', berat_satuan = {beratSatuan}, harga_satuan = {hargaSatuan}, kode_tipe_item = '{kodeTipeItem}' "
                command += f"WHERE kode = '{id}'"
                print(command)
                c.execute(command)
            return redirect('item:all')
            
        else:
            with connection.cursor() as c:
                command = f"select * from item_sumber_daya where item_sumber_daya.kode = '{id}'"
                c.execute(command)
                dataSQL = c.fetchall()
                context["dataItem"] = dataSQL[0]

                command2 = "select * from tipe_item"
                c.execute(command2)
                dataTipe = c.fetchall()
                context["dataTipe"] = dataTipe
                context["dataJsonTipe"] = json.dumps(dataTipe)

                command3 =  f"select nama from tipe_item where kode in (select kode_tipe_item from item_sumber_daya where kode = '{id}')"
                c.execute(command3)
                dataSQL = c.fetchone()
                context["currentTipeItem"] = dataSQL[0]
            return render(request, 'item/update.html', context)
    else:
        return redirect('/')

# See 5f
#Belum bisa munculin notifikasi berhasil dihapus
def delete(request, id):
    if request.session.role == "Supplier":
        with connection.cursor() as c:
            command = f"delete from item_sumber_daya where kode = '{id}'"
            c.execute(command)
            print(command)
        return redirect('item:all')
    else:
        return redirect('/')