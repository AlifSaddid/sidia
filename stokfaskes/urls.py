from django.urls import path
from . import views

app_name = 'stokfaskes'

urlpatterns = [
    path('new', views.new, name = 'new'),
    path('update/<str:idfaskes>/<str:idbarang>', views.update, name = 'update'),
    path('delete/<str:idfaskes>/<str:idbarang>', views.delete, name = 'delete'),
    path('', views.all, name = 'all')
]