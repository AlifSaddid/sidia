from django.shortcuts import redirect, render
from django.db import connection

# See 8c (Done)
def new(request):
    try:
        if (request.session['username'] == ''):
            return redirect ('/authentication/login')    
    except:
        return redirect ('/authentication/login')

    if (request.method == 'GET'):
        return display_new(request)
    else:
        return save_new(request)

def display_new(request):
    context = {
        'all_faskes': convert_to_list(get_all_faskes()),
        'items': convert_to_list(get_all_item_sumber_daya())
    }
    return render(request, 'stokfaskes/new.html', context)

def save_new(request):
    data = request.POST
    kode_faskes = data['kode-faskes']
    kode_item = data['kode-item']
    jumlah = data['jumlah']
    with connection.cursor() as c:
        c.execute('''
            INSERT INTO STOK_FASKES(Kode_faskes, Kode_item_sumber_daya, Jumlah)
            VALUES ('{}', '{}', {})
        '''.format(kode_faskes, kode_item, jumlah))
    return render(request, 'stokfaskes/new.html')

# See 8d (Done)
def all(request):
    try:
        if (request.session['username'] == ''):
            return redirect ('/authentication/login')    
    except:
        return redirect ('/authentication/login')
    context = {
        'all_stok': convert_to_list(get_all_stok())
    }
    return render(request, 'stokfaskes/all.html', context)

# See 8e (Done)
def update(request, idfaskes, idbarang):
    try:
        if (request.session['username'] == ''):
            return redirect ('/authentication/login')    
    except:
        return redirect ('/authentication/login')
    if (request.method == 'POST'):
        return save_update(request, idfaskes, idbarang)
    else:
        return display_update(request, idfaskes, idbarang)

def display_update(request, idfaskes, idbarang):
    context = {}
    with connection.cursor() as c:
        c.execute('''
            SELECT sf.*, LOKASI.provinsi, LOKASI.kabkot, TIPE_FASKES.nama_tipe, isd.nama FROM stok_faskes sf
            LEFT JOIN faskes fsk
                ON sf.kode_faskes = fsk.kode_faskes_nasional
            LEFT JOIN LOKASI
                ON LOKASI.id = fsk.id_lokasi
            LEFT JOIN TIPE_FASKES
                ON TIPE_FASKES.kode = fsk.kode_tipe_faskes
            LEFT JOIN item_sumber_daya isd
            ON isd.kode = sf.kode_item_sumber_daya
            WHERE sf.kode_faskes = '{}' AND sf.kode_item_sumber_daya = '{}'
        '''.format(idfaskes, idbarang))

        context['item'] = c.fetchall()[0]
    return render(request, 'stokfaskes/update.html', context)

def save_update(request, idfaskes, idbarang):
    jumlah = request.POST['jumlah']
    with connection.cursor() as c:
        c.execute('''
            UPDATE STOK_FASKES SET jumlah = {}
            WHERE kode_faskes = '{}' AND kode_item_sumber_daya = '{}'
        '''.format(jumlah, idfaskes, idbarang))
    return render(request, 'stokfaskes/update.html')

# See 8f (Done)
def delete(request, idfaskes, idbarang):
    try:
        if (request.session['username'] == ''):
            return redirect ('/authentication/login')    
    except:
        return redirect ('/authentication/login')
    with connection.cursor() as c:
        c.execute('''
            DELETE FROM stok_faskes
            WHERE kode_faskes = '{}' AND kode_item_sumber_daya = '{}'
        '''.format(idfaskes, idbarang))
    return render(request, 'stokfaskes/all.html')

# Utils
def convert_to_list(tuples):
    result = []
    for data in tuples:
        result.append(list(data))
    return result

def get_all_stok():
    with connection.cursor() as c:
        c.execute('''
            SELECT sf.*, LOKASI.provinsi, LOKASI.kabkot, TIPE_FASKES.nama_tipe, isd.nama FROM stok_faskes sf
            LEFT JOIN faskes fsk
                ON sf.kode_faskes = fsk.kode_faskes_nasional
            LEFT JOIN LOKASI
                ON LOKASI.id = fsk.id_lokasi
            LEFT JOIN TIPE_FASKES
                ON TIPE_FASKES.kode = fsk.kode_tipe_faskes
            LEFT JOIN item_sumber_daya isd
            ON isd.kode = sf.kode_item_sumber_daya
        ''')
        return c.fetchall()

def get_all_faskes():
    with connection.cursor() as c:
        c.execute('''
            SELECT FASKES.*, LOKASI.provinsi, LOKASI.kabkot, TIPE_FASKES.nama_tipe FROM FASKES
            LEFT JOIN LOKASI
                ON LOKASI.id = FASKES.id_lokasi
            LEFT JOIN TIPE_FASKES
                ON TIPE_FASKES.kode = FASKES.kode_tipe_faskes
        ''')

        return c.fetchall()

def get_all_item_sumber_daya():
    with connection.cursor() as c:
        c.execute('''
            SELECT * FROM item_sumber_daya
        ''')
        
        return c.fetchall()