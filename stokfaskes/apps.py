from django.apps import AppConfig


class StokfaskesConfig(AppConfig):
    name = 'stokfaskes'
