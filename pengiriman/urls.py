from django.urls import path
from . import views

app_name = 'pengiriman'

urlpatterns = [
    path('new', views.new, name = 'new'),
    path('status/new/<str:id>', views.statuscreate, name = 'statuscreate'),
    path('status/riwayat/<str:id>', views.status, name = 'status'),
    path('', views.all, name = 'all')
]