from django.shortcuts import render, redirect
from django.db import connection
import json
import datetime
# See 14d
def new(request):
    return render(request, 'pengiriman/new.html')

# See 14d
def all(request):
    return render(request, 'pengiriman/all.html')

# See 17c
def statuscreate(request, id):
    data = []
    context = {}

    if(request.session["role"] == "Admin Satgas"):
        if(request.method == "POST"):
            kodeStatusBatch = request.POST["kodeStatusBatch"]
            tanggal = f"{datetime.datetime.now():%Y-%m-%d}"
            with connection.cursor() as c:
                for batch in getAllBatchCodeInOnePermohonan(id):
                    command = "INSERT INTO RIWAYAT_STATUS_PENGIRIMAN(Kode_status_batch_pengiriman, Kode_batch, Tanggal) VALUES "
                    command += str((str(kodeStatusBatch), batch, tanggal))
                    c.execute(command)
            return render(request, 'pengiriman/statusnew.html')
        else:
            context["pilihanKodeStatus"] = getChoices(getLastUpdatedStatusBatch(id))
            context["kodeTransaksi"] = json.dumps(id)
            return render(request, 'pengiriman/statusnew.html', context)
    else:
        return redirect("/")

#Ambil Status Terakhir dari suatu permohonan di batch tertentu
#1 permohonan punya banyak batch dan perubahan status batch dilakukan serentak untuk 1 permohonan
def getLastUpdatedStatusBatch(id):
    with connection.cursor() as c:
        command = "select kode_status_batch_pengiriman from riwayat_status_pengiriman "
        command += f"where kode_batch in (select kode from batch_pengiriman where nomor_transaksi_sumber_daya = {id})"
        command += "order by tanggal desc"
        c.execute(command)
        kodeStatus = c.fetchone()
        return kodeStatus[0]

#Ambil kode-kode batch di 1 permohonan
def getAllBatchCodeInOnePermohonan(id):
    data = []
    with connection.cursor() as c:
        command = f"select kode from batch_pengiriman where nomor_transaksi_sumber_daya = {id}"
        c.execute(command)
        for i in c.fetchall():
            data.append(i[0])
        return data

def getChoices(currentStatus):
    choices = []
    if(currentStatus == "PRO"):
        choices.append("dalam perjalanan (OTW)")
    elif(currentStatus == "OTW"):
        choices.append("selesai (DLV)")
        choices.append("hilang (HLG)")
        choices.append("dikembalikan (RET)")
    return choices


# See 17d
def status(request, id):
    data = []
    context = {}
    if(request.session['role'] == "Admin Satgas"):
        with connection.cursor() as c:
            command = "select rsp.kode_status_batch_pengiriman, sbp.nama, bp.username_satgas, rsp.tanggal "
            command += "from transaksi_sumber_daya tsd, batch_pengiriman bp, riwayat_status_pengiriman rsp, status_batch_pengiriman sbp "
            command += f"where sbp.kode = rsp.kode_status_batch_pengiriman and bp.kode = rsp.kode_batch and tsd.nomor = bp.nomor_transaksi_sumber_daya and bp.nomor_transaksi_sumber_daya = {id}"
            command += "order by rsp.tanggal desc"
            c.execute(command)
            dataSQL = c.fetchall()
            
            counter = 0
            for i in dataSQL:
                counter += 1
                data.append((counter, i))
            context["nomorTransaksi"] = id
            context["data"] = data
        return render(request, 'pengiriman/statusriwayat.html', context)
    elif(request.session['role'] == "Petugas Distribusi"):
        username = request.session['username']
        with connection.cursor() as c:
            command = "select rsp.kode_status_batch_pengiriman, sbp.nama, bp.username_satgas, rsp.tanggal "
            command += "from transaksi_sumber_daya tsd, batch_pengiriman bp, riwayat_status_pengiriman rsp, status_batch_pengiriman sbp "
            command += f" where sbp.kode = rsp.kode_status_batch_pengiriman and bp.kode = rsp.kode_batch and tsd.nomor = bp.nomor_transaksi_sumber_daya and bp.username_petugas_distribusi = '{username}' and bp.nomor_transaksi_sumber_daya = {id}"
            command += "order by rsp.tanggal desc"
            c.execute(command)
            dataSQL = c.fetchall()
            
            counter = 0
            for i in dataSQL:
                counter += 1
                data.append((counter, i))
            context["nomorTransaksi"] = id
            context["data"] = data
            context["dataNull"] = len(data) == 0
        return render(request, 'pengiriman/statusriwayat.html', context)

    else:
        return redirect("/")