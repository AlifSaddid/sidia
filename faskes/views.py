from django.shortcuts import render

# See 12c
def new(request):
    return render(request, 'faskes/new.html')

# See 12d
def all(request):
    return render(request, 'faskes/all.html')

# See 12e
def update(request, id):
    return render(request, 'faskes/update.html')

# See 12f
def delete(request, id):
    return render(request, 'faskes/all.html')