from django.urls import path
from . import views

app_name = 'faskes'

urlpatterns = [
    path('new', views.new, name = 'new'),
    path('update/<str:id>', views.update, name = 'update'),
    path('delete/<str:id>', views.delete, name = 'delete'),
    path('', views.all, name = 'all')
]