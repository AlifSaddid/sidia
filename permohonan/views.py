from django.shortcuts import render

# See 3c
def new(request):
    return render(request, 'permohonan/new.html')

# See 3d
def all(request):
    return render(request, 'permohonan/all.html')

# See 3d
def detail(request, id):
    return render(request, 'permohonan/detail.html')

# See 3e
def update(request, id):
    return render(request, 'permohonan/update.html')

# See 3f
def delete(request, id):
    return render(request, 'permohonan/all.html')

# See 4c
def statusnew(request, id):
    return render(request, 'permohonan/all.html')

# See 4d
def status(request, id):
    return render(request, 'permohonan/status.html')
