from django.urls import path
from . import views

app_name = 'permohonan'

urlpatterns = [
    path('new', views.new, name = 'new'),
    path('detail/<str:id>', views.detail, name = 'detail'),
    path('update/<str:id>', views.update, name = 'update'),
    path('delete/<str:id>', views.delete, name = 'delete'),
    path('status/<str:id>/new', views.statusnew, name = 'req'),
    path('status/<str:id>', views.status, name = 'status'),
    path('', views.all, name = 'all')
]