from django.shortcuts import render, redirect
from django.db import connection
import json

def new(request, id):
    context = {}
    if(request.session["role"] == "Admin Satgas"):
        if(request.method == "POST"):
            kodeBatch = request.POST["kodeBatch"]
            usernameSatgas = request.POST["usernameSatgas"]
            usernamePDistribusi = request.POST["usernamePDistribusi"]
            noTransaksi = request.POST["noTransaksi"]
            kodeKendaraan = request.POST["kodeKendaraan"]
            tandaTerima = request.POST["tandaTerima"]
            with connection.cursor() as c:
                command = "insert into batch_pengiriman (Kode, Username_satgas, Username_petugas_distribusi, Tanda_terima, Nomor_transaksi_sumber_daya, No_kendaraan) "
                command += f"values ('{kodeBatch}', '{usernameSatgas}', '{usernamePDistribusi}', '{tandaTerima}', {noTransaksi}, '{kodeKendaraan}')"
                c.execute(command)
                return render(request, 'batch/new.html')
        else:
            context["nextBatchCode"] = getNextBatchCode()
            context["petugasSatgas"] = request.session["username"]
            context["noTransaksi"] = id
            context["jsonNoTransaksi"] = json.dumps(id)
            context["jsonPetugasSatgas"] = json.dumps(request.session["username"])
            context["pilihanKendaraan"] = getKendaraan(id)
            context["pilihanPetugasDistribusi"] = getPetugasDistribusi(id)
            context["daftarBatchPengiriman"] = getDaftarBatchPengiriman(id, request.session["role"], request.session["username"])
            return render(request, 'batch/new.html', context)
    else:
        return redirect("/")

def getNextBatchCode():
    with connection.cursor() as c:
        c.execute("select * from batch_pengiriman")
        return "BTC" + str(len(c.fetchall()) + 1)

def getKendaraan(id):
    data = {}
    with connection.cursor() as c:
        command = "select distinct (k.*) from kendaraan k, batch_pengiriman bp, riwayat_status_pengiriman rsp "
        command += "where k.nomor = bp.no_kendaraan and bp.kode = rsp.kode_batch and bp.kode in( "
        command += "select distinct on (rsp.kode_batch) rsp.kode_batch where rsp.kode_status_batch_pengiriman = 'DLV' or rsp.kode_status_batch_pengiriman = 'HLG' or rsp.kode_status_batch_pengiriman = 'RET' order by rsp.kode_batch, rsp.tanggal) "
        command += "except "
        command += "(SELECT k.nomor, k.nama, k.jenis_kendaraan, k.berat_maksimum FROM KENDARAAN k "
        command += f"JOIN BATCH_PENGIRIMAN bp ON bp.No_kendaraan = k.nomor WHERE nomor_transaksi_sumber_daya = {id})"
        c.execute(command)
        for i in c.fetchall():
            data[i[0]] = f"{i[0]} - {i[1]} ({i[2]}) dengan berat maksimum: {i[3]} kg"
    return data

def getPetugasDistribusi(id):
    data = {}
    with connection.cursor() as c:
        command = "select distinct (pd.*) from petugas_distribusi pd, batch_pengiriman bp, riwayat_status_pengiriman rsp "
        command += "where bp.kode = rsp.kode_batch and bp.kode in( "
        command += "select distinct on (rsp.kode_batch) rsp.kode_batch where rsp.kode_status_batch_pengiriman = 'DLV' or rsp.kode_status_batch_pengiriman = 'HLG' or rsp.kode_status_batch_pengiriman = 'RET' order by rsp.kode_batch, rsp.tanggal) "
        command += "except "
        command += f"(select pd.username, pd.no_sim from petugas_distribusi pd, batch_pengiriman bp where pd.username = bp.username_petugas_distribusi and bp.nomor_transaksi_sumber_daya = {id});"
        c.execute(command)
        for i in c.fetchall():
            data[i[0]] = f"{i[0]} - {i[1]}"
    return data

def detailAll(request):
    #Yang bisa: Admin Satgas (terdapat validasi ketersediaan stock)
    context = {}
    if(request.session['role'] in ["Admin Satgas", "Petugas Distribusi", "Petugas Faskes"]):
        context["dataPermohonan"] = getTable(request.session["role"], request.session["username"])
        context["role"] = request.session["role"]
        return render(request, 'batch/all.html', context)
    else:
        return redirect("/")

def getTable(role, username):
    kodeTransaksi = []
    data = []
    with connection.cursor() as c:
        command = "select distinct on (psdf.no_transaksi_sumber_daya) psdf.no_transaksi_sumber_daya, p.nama, f.kode_faskes_nasional, tsd.total_berat, rsp.kode_status_permohonan, rsp.tanggal "
        command += "from permohonan_sumber_daya_faskes psdf, pengguna p, faskes f, transaksi_sumber_daya tsd, riwayat_status_permohonan rsp "
        command += "where psdf.no_transaksi_sumber_daya = tsd.nomor and psdf.username_petugas_faskes = p.username and f.username_petugas = psdf.username_petugas_faskes and psdf.no_transaksi_sumber_daya = rsp.nomor_permohonan "
        command += "and psdf.no_transaksi_sumber_daya in ( select rsp.nomor_permohonan from riwayat_status_permohonan rsp where rsp.kode_status_permohonan = 'PRO') "
        if(role == "Petugas Faskes"):
            command += f"and psdf.username_petugas_faskes = '{username}' "
        elif(role == "Petugas Distribusi"):
            command += f"and psdf.no_transaksi_sumber_daya in ( select distinct(bp.nomor_transaksi_sumber_daya) from batch_pengiriman bp where bp.username_petugas_distribusi = '{username}') "        
        command += "order by psdf.no_transaksi_sumber_daya, rsp.tanggal desc"
        c.execute(command)
        dataSQL = c.fetchall()
        for i in dataSQL:
            kodeTransaksi.append(i[0])
            data.append(i)
        
        dataStatusBatch = getStatusBatch(kodeTransaksi)
        for i in range(len(data)):
            data[i] = list(data[i])
            data[i].append(dataStatusBatch[i])
            data[i] = tuple(data[i])
        return data

def getStatusBatch(listKodeTransaksi):
    dataKodeStatusBatch = []
    with connection.cursor() as c:
        for kode in listKodeTransaksi:
            command = "select rsp.kode_status_batch_pengiriman from batch_pengiriman bp, riwayat_status_pengiriman rsp "
            command += f"where bp.kode = rsp.kode_batch and bp.nomor_transaksi_sumber_daya = {kode}"
            command += "order by rsp.tanggal desc limit 1"
            c.execute(command)
            data = c.fetchone()
            if(data):
                dataKodeStatusBatch.append(data[0])
            else:
                dataKodeStatusBatch.append("-") #Kalau status batchnya belum ada
        return dataKodeStatusBatch


# See 16g
def detailWithId(request, id):
    context = {}
    if(request.session['role'] == "Admin Satgas"):
        context["petugasSatgas"] = getPetugasSatgas(id)
        context["nomorTransaksi"] = id
        context["dataBatch"] = getDaftarBatchPengiriman(id, "Admin Satgas", request.session["username"])
        return render(request, 'batch/detail.html', context)
    elif(request.session['role'] == "Petugas Distribusi"):
        context["petugasSatgas"] = getPetugasSatgas(id)
        context["nomorTransaksi"] = id
        context["dataBatch"] = getDaftarBatchPengiriman(id, "Petugas Distribusi", request.session["username"])
        return render(request, 'batch/detail.html', context)
    elif(request.session['role'] == "Petugas Faskes"):
        context["petugasSatgas"] = getPetugasSatgas(id)
        context["nomorTransaksi"] = id
        context["dataBatch"] = getDaftarBatchPengiriman(id, "Petugas Faskes", request.session["username"])

        return render(request, 'batch/detail.html', context)
    else:
        return redirect("/")

def getPetugasSatgas(id):
    with connection.cursor() as c:
        command = "select distinct p.nama from batch_pengiriman bp, pengguna p where p.username = bp.username_satgas"
        command += f" and bp.nomor_transaksi_sumber_daya = {id}"
        c.execute(command)
        dataPetugas = c.fetchone()
        return dataPetugas[0]

def getDaftarBatchPengiriman(id, role, username):
    data = []
    dataKodeBatch = []
    command = "select bp.kode, k.nama, p.nama from batch_pengiriman bp, transaksi_sumber_daya tsd, kendaraan k, pengguna p "
    command += f"where bp.no_kendaraan = k.nomor and bp.username_petugas_distribusi = p.username and tsd.nomor = bp.nomor_transaksi_sumber_daya and bp.nomor_transaksi_sumber_daya ={id} "
    with connection.cursor() as c:
        if(role == "Admin Satgas"):
            c.execute(command)
        elif(role == "Petugas Distribusi"):
            command += f"and bp.username_petugas_distribusi = '{username}'"
            c.execute(command)
        elif(role == "Petugas Faskes"): #Kalo petugas faskes ngequery nya dari permohonan sumber daya faskes
            command = "select bp.kode, k.nama, p.nama from batch_pengiriman bp, permohonan_sumber_daya_faskes psdf, kendaraan k, pengguna p "
            command += f"where bp.no_kendaraan = k.nomor and bp.username_petugas_distribusi = p.username and psdf.no_transaksi_sumber_daya = bp.nomor_transaksi_sumber_daya and bp.nomor_transaksi_sumber_daya ={id} "
            command += f"and psdf.username_petugas_faskes = '{username}'"
            c.execute(command)

        for i in c.fetchall():
            data.append(i)
            dataKodeBatch.append(i[0])
            
        dataLokasiAsal = getLokasiAsal(dataKodeBatch)
        dataLokasiTujuan = getLokasiTujuan(dataKodeBatch)

        for i in range(len(data)):
            data[i] = list(data[i])
            data[i].append(dataLokasiAsal[i])
            data[i].append(dataLokasiTujuan[i])
            data[i] = tuple(data[i])
        return data

#Get Lokasi Asal (Alamat Lengkap) by kode batch
def getLokasiAsal(listKodeBatch):
    data = []
    with connection.cursor() as c:
        command = "select l.* from lokasi l, batch_pengiriman bp"
        for kode in listKodeBatch:
            command += f" where bp.id_lokasi_asal = l.id and bp.kode = '{kode}'"
            c.execute(command)
            for i in c.fetchall():
                data.append(f"{i[5]}, Kel. {i[4]}, Kec. {i[3]}, Kab/Kota {i[2]}, Prov {i[1]}")
    return data

#Get Lokasi Tujuan (Alamat Lengkap) by kode batch
def getLokasiTujuan(listKodeBatch):
    data = []
    with connection.cursor() as c:
        command = "select l.* from lokasi l, batch_pengiriman bp"
        for kode in listKodeBatch:
            command += f" where bp.id_lokasi_tujuan = l.id and bp.kode = '{kode}'"
            c.execute(command)
            for i in c.fetchall():
                data.append(f"{i[5]}, Kel. {i[4]}, Kec. {i[3]}, Kab/Kota {i[2]}, Prov {i[1]}")
    return data
