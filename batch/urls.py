from django.urls import path
from . import views

app_name = 'batch'

urlpatterns = [
    path('new/<str:id>', views.new, name = 'new'),
    path('', views.detailAll, name = 'detailAll'),
    path('<str:id>', views.detailWithId, name = 'detailId')
]