from django.urls import path
from . import views

app_name = 'stokwarehouse'

urlpatterns = [
    path('new', views.new, name = 'new'),
    path('update/<str:id_lokasi>/<str:id_item>', views.update, name = 'update'),
    path('delete/<str:id_lokasi>/<str:id_item>', views.delete, name = 'delete'),
    path('', views.all, name = 'all')
]