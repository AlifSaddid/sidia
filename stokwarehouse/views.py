from django.shortcuts import render, redirect
from django.db import connection
import json

# See 15c
#Fetch data done and Add new stok_warehouse_provinsi done
def new(request):
    context = {}
    data = []
    data2 = []
    if(request.session["role"] == "Admin Satgas"):
        if(request.method == "POST"):
            idLokasi = request.POST['idLokasi']
            kodeItem = request.POST['kodeItem']
            jumlahItem = request.POST['jumlahItem']
            with connection.cursor() as c:
                command = "INSERT INTO STOK_WAREHOUSE_PROVINSI VALUES "
                command += str((str(idLokasi), jumlahItem, str(kodeItem)))
                c.execute(command)
            return redirect('stokwarehouse:all')
        else:
            with connection.cursor() as c:
                c.execute("select * from lokasi")
                dataSQL = c.fetchall()
                for x in dataSQL:
                    alamat = x[5] + ", Kel. " + x[4] + ", Kec. " + x[3] + ", Kab/Kota " + x[2] + ", Prov " + x[1]
                    data.append((x[0], alamat))
                context["dataLokasi"] = data
                context["dataJsonLokasi"] = json.dumps(data)

                c.execute("select kode, nama from item_sumber_daya")
                dataSQL = c.fetchall()
                for x in dataSQL:
                    data2.append((x[0], x[1]))
                context["dataItem"] = data2
                context["dataJsonItem"] = json.dumps(data2)
            return render(request, 'stokwarehouse/new.html', context)
    else:
        return redirect('/')

# See 15d
# Untuk admin satgas bisa create, update, dan read semua stok warehouse pada satgas
# Petugas faskes hanya bisa read stok pada warehouse satgas yang sama dengan provinsi dari Faskes tersebut
def all(request):
    # Read stock -> 
    # Admin satgas bisa liat semua
    # Petugas Faskes hanya bisa melihat stock item sumber daya pada warehouse satgas yang sama dengan provinsi tersebut
    data = []
    context = {}
    if request.session["role"] == "Admin Satgas":
        with connection.cursor() as c:
            command = "select swp.*, lokasi.provinsi, lokasi.kabkot, isd.nama from stok_warehouse_provinsi swp "
            command += "left join warehouse_provinsi wp on swp.id_lokasi_warehouse = wp.id_lokasi "
            command += "left join lokasi on lokasi.id = wp.id_lokasi "
            command += "left join item_sumber_daya isd on isd.kode = swp.kode_item_sumber_daya;"
            c.execute(command)
            dataSQL = c.fetchall()
        dictLokasi = {}
        for i in dataSQL:
            if i[0] not in dictLokasi:
                dictLokasi[i[0]] = "Table Stock Warehouse Satgas Prov. " + i[3] + ", Kab./Kota " + i[4]
        context["dictLokasi"] = dictLokasi
        context["data"] = dataSQL
        context["dataJson"] = json.dumps(dataSQL)
        context["role"] = request.session["role"]
        return render(request, 'stokwarehouse/all.html', context)
    elif request.session["role"] == "Petugas Faskes":
        username = request.session["username"]
        with connection.cursor() as c:
            command = "select swp.*, l.provinsi, l.kabkot, isd.nama from stok_warehouse_provinsi as swp, item_sumber_daya as isd, lokasi as l"
            command += " where swp.kode_item_sumber_daya = isd.kode and swp.id_lokasi_warehouse = l.id"
            command += f" and swp.id_lokasi_warehouse in (select id_lokasi from faskes where username_petugas = '{username}' and id_lokasi = swp.id_lokasi_warehouse)"
            c.execute(command)
            dataSQL = c.fetchall()
        dictLokasi = {}
        for i in dataSQL:
            if i[0] not in dictLokasi:
                dictLokasi[i[0]] = "Table Stock Warehouse Satgas Prov. " + i[3] + ", Kab./Kota " + i[4]
        context["dictLokasi"] = dictLokasi
        context["data"] = dataSQL
        context["dataJson"] = json.dumps(dataSQL)
        context["role"] = request.session["role"]
        return render(request, 'stokwarehouse/all.html', context)

# See 15e
def update(request, id_lokasi, id_item):
    data = []
    context = {}
    if request.session["role"] == "Admin Satgas":
        if(request.method == "POST"):
            jumlahItem = request.POST['jumlahItem']
            with connection.cursor() as c:
                command = f"update stok_warehouse_provinsi set jumlah = {jumlahItem} "
                command += f"where id_lokasi_warehouse = '{id_lokasi}' and kode_item_sumber_daya = '{id_item}'"
                c.execute(command)
            return redirect('stokwarehouse:all')
        else:
            with connection.cursor() as c:
                command = f"select * from stok_warehouse_provinsi where id_lokasi_warehouse = '{id_lokasi}' and kode_item_sumber_daya = '{id_item}'"
                c.execute(command)
                dataSQL = c.fetchall()
                for x in dataSQL:
                    data.append((x))
                
                commandLokasi = f"select * from lokasi where id = '{id_lokasi}'"
                c.execute(commandLokasi)
                dataSQL2 = c.fetchone()
                dataLokasi = dataSQL2[5] + ", Kel. " + dataSQL2[4] + ", Kec. " + dataSQL2[3] + ", Kab/Kota " + dataSQL2[2] + ", Prov " + dataSQL2[1]

                commandItem = f"select nama from item_sumber_daya where kode = '{id_item}'"
                c.execute(commandItem)
                dataSQL3 = c.fetchone()
                dataItem = dataSQL3[0]

            context["data"] = data
            context["lokasi"] = dataLokasi
            context["item"] = dataItem
            return render(request, 'stokwarehouse/update.html', context)
    else:
        return redirect('/')


def delete(request, id_lokasi, id_item):
    if request.session["role"] == "Admin Satgas":
        with connection.cursor() as c:
            command = f"delete from stok_warehouse_provinsi where id_lokasi_warehouse = '{id_lokasi}' and kode_item_sumber_daya = '{id_item}'"
            c.execute(command)
        return render(request, 'stokwarehouse/all.html')
    else:
        return redirect('/')