from django.apps import AppConfig


class StokwarehouseConfig(AppConfig):
    name = 'stokwarehouse'
