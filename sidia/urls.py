from django.contrib import admin
from django.urls import path
from django.urls.conf import include
from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('authentication/', include('authentication.urls', namespace = 'authentication')),
    path('batch/', include('batch.urls', namespace = 'batch')),
    path('faskes/', include('faskes.urls', namespace = 'faskes')),
    path('item/', include('item.urls', namespace = 'item')),
    path('pengiriman/', include('pengiriman.urls', namespace = 'pengiriman')),
    path('permohonan/', include('permohonan.urls', namespace = 'permohonan')),
    path('pesanan/', include('pesanan.urls', namespace = 'pesanan')),
    path('stok-faskes/', include('stokfaskes.urls', namespace = 'stokfaskes')),
    path('stok-warehouse/', include('stokwarehouse.urls', namespace = 'stokwarehouse')),
    path('warehouse/', include('warehouse.urls', namespace = 'warehouse')),
    path('', views.homepage)
]
